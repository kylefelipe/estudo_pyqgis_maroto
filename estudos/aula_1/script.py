# Importando o OGR.

from osgeo import ogr

gpkgPath = "/dados/bcim_2016_21_11_2018.gpkg|lim_unidade_federacao_a"

if QgsVectorLayer("/dados/bcim_2016_21_11_2018.gpkg|lim_unidade_federacao_a", 'lim_unidade_federacao_a', 'ogr').isValid():
    print( "Camada é Válida" )
else:
    print( 'Camada não é várlda' )

# Carregando um dado vetorial
iface.addVectorLayer("/dados/bcim_2016_21_11_2018.gpkg|layername=lim_unidade_federacao_a", "lim_unidade_federacao_a", "ogr")

# Conectando ao arquivo gpkg
con = ogr.Open(gpkgPath)

# Imprimindo os nomes das camadas
for i in con:
    print(i.GetName())

# Adicionando as camadas ao projeto:

for i in con:
    vectorLayerPath = gpkgPath + "|layername=" + i.GetName()
    basename = i.GetName()
    iface.addVectorLayer(vectorLayerPath, basename, 'ogr')

uf = iface.activeLayer()

# Contando as feições
uf.featureCount()

# Mostrando os campos.
for field in uf.fields():
    print(field.name())

# Mostrando o nome do primeiro campo
print(uf.fields()[0].name())

# Abrindo a tabela de atributos
iface.showAttributeTable(uf)

for feature in uf.getFeatures():
    print(feature["nome"])

# Filtrando os dados da chamada.
rj = uf.setSubsetString("\"nome\" = 'Rio de Janeiro'")

# Limpando a o filtro.
uf.setSubsetString("")

# Alterando sibologia
uf.renderer().symbol().setColor(QColor("black"))

# Atualizando a lista de camadas
iface.layerTreeView().refreshLayerSymbology(uf.id())

# Salvando o projeto
QgsProject.instance().write('/projetos/projeto1.qgs')

# Exibindo o nome do arquivo de projeto
print(QgsProject.instance().fileName())

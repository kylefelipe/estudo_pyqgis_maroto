# QUEBRANDO O GELO

Vamos quebrar o gelo, dando os primeiros passos na API Python do Qgis:

O Qgis possui um terminal integrado onde, através dos comandos, podemos
adicionar camadas e interagir com as camadas existentes.  
Podemos rodar e criar scripts python.  
Para acessá-lo basta irmos em Menu Complementos > Teminal Python  
Atalho: Ctrl + Alt + p  

Digite, na frente do >>>, o seguinte comando  

```python
print( 'Hello World!' )
```
Ou se preferir em Pt_Br:  

```python
print( 'Olá Mundo!' )
```
Parabéns, esse foi o primeiro script rodado no Qgis, simples, não é?!  

> "Mas a vida é uma caixinha de surpresas!". NARRADOR, Joseph Climber.   

Os comandos das lições a seguir serão utilizados no terminal iterativo.
Algumas precauções devem ser tomadas:  

Lembre-se que no python a identação do código conta, então lembre-se de
utilizar sempre que necessário.
Adote um padrão: tabulação, 4 espaços. Utilize-os o tempo todo.

Exemplo:

```Python
for i in 'alo':
  print( i ) # antes de digitar a linha, foi pressionado a tecla TAB
```

ou  

```Python
for i in algo:
    print( i ) # antes de digitar a linha, foi pressionado a tecla espaço 4x
```

Quando for rodar o script acima a tecla enter deverá ser pressionada 2x para
executar o comando.

Futuramente vamos colocar nossos códigos dentro de um arquivo.py e executar esse script.  


# 1. Abrindo um arquivo GEOPACKAGE

Há algumas formas de fazer isso:  
* Com o [`QgsVectorLayer()`](QgsVectorLayer), demanda a importação dessa classe
(`from qgis.core import QgsVectorLayer`), e a importação ficaria da seguinte
forma: `QgsVectorLayer(path, baseName, providerLib, options)`;  
Ex.: `QgsVectorLayer("./dados/gpkg_places_layer", "Places layer", "ogr")`  

Exemplo:  

```Python
QgsVectorLayer("/testes_pyqgis/dados/bcim_2016_21_11_2018.gpkg", 'lim_unidade_federacao_a', 'ogr')
```

Esse métdo não adiciona a camada ao projeto, ele o acessa e transforma em um
objeto "QgsVectorLayer".  
Isso é muito útil se quisermos saber se a camada é válida antes de adicionar
ao projeto, por exemplo.

```Python
if QgsVectorLayer("/testes_pyqgis/dados/bcim_2016_21_11_2018.gpkg", 'lim_unidade_federacao_a', 'ogr').isValid():
  print( "Essa camada é válida")
```

* Outra possibilidade é usando o [`addVectorLayer()`](https://qgis.org/pyqgis/master/gui/QgisInterface.html#qgis.gui.QgisInterface.addVectorLayer), que é um método do [`QgisInterface`](https://qgis.org/pyqgis/master/gui/QgisInterface.html#qgis.gui.QgisInterface), este último podendo ser "abreviado" como `iface`; A importação depende das seguintes variáveis `addVectorLayer(vectorLayerPath, baseName, providerKey)` (bem similar ao `qgsVectorLayer`.  
O Metodo `addVectorLayer` retorna um objeto `QgsVectorLayer` e o adiciona
automáticamente na lista de mapLayers do Qgis;  

> iface is an object belonging to QGIS – something that has properties and behaviors we can use to interact with QGIS. iface is a very important object in QGIS because without it, we could not interact with QGIS or any layers loaded in our project. (Anita Graser)

**ATENÇÃO**  
* *vectorLayerPath*: deve incluir o nome da layer a ser carregada, precedida de `|layername=`. E.g.: `bcim_2016_21_11_2018.gpkg|layername=lim_unidade_federacao_a`  
* *basename*: nome a ser dado à layer no projeto qgis;  
* *providerKey*: "ogr"  
Outros podem ser usados, como -“postgres”, “delimitedtext”, “gpx”, “spatialite”, and “WFS”.  

Exemplo:  

```Python
iface.addVectorLayer("/testes_pyqgis/dados/bcim_2016_21_11_2018.gpkg|layername=lim_unidade_federacao_a", 'lim_unidade_federacao_a', 'ogr' )
```

## Adicionando uma camada

Há algumas formas de fazer isso:  
* Com o [`QgsVectorLayer()`](QgsVectorLayer), demanda a importação dessa classe
(`from qgis.core import QgsVectorLayer`), e a importação ficaria da seguinte
forma: `QgsVectorLayer(path, baseName, providerLib, options)`;  
Ex.: `QgsVectorLayer("./dados/gpkg_places_layer", "Places layer", "ogr")`  

Exemplo:  

```Python
QgsVectorLayer("/testes_pyqgis/dados/bcim_2016_21_11_2018.gpkg", 'lim_unidade_federacao_a', 'ogr')
```

Esse métdo não adiciona a camada ao projeto, ele o acessa e transforma em um
objeto "QgsVectorLayer".  
Isso é muito útil se quisermos saber se a camada é válida antes de adicionar
ao projeto, por exemplo.

```Python
if QgsVectorLayer("/testes_pyqgis/dados/bcim_2016_21_11_2018.gpkg", 'lim_unidade_federacao_a', 'ogr').isValid():
  print( "Essa camada é válida")
```

* Outra possibilidade é usando o [`addVectorLayer()`](https://qgis.org/pyqgis/master/gui/QgisInterface.html#qgis.gui.QgisInterface.addVectorLayer), que é um método do [`QgisInterface`](https://qgis.org/pyqgis/master/gui/QgisInterface.html#qgis.gui.QgisInterface), este último podendo ser "abreviado" como `iface`; A importação depende das seguintes variáveis `addVectorLayer(vectorLayerPath, baseName, providerKey)` (bem similar ao `qgsVectorLayer`.  
O Metodo `addVectorLayer` retorna um objeto `QgsVectorLayer` e o adiciona
automáticamente na lista de mapLayers do Qgis;  

> iface is an object belonging to QGIS – something that has properties and behaviors we can use to interact with QGIS. iface is a very important object in QGIS because without it, we could not interact with QGIS or any layers loaded in our project. (Anita Graser)

**ATENÇÃO**  
* *vectorLayerPath*: deve incluir o nome da layer a ser carregada, precedida de `|layername=`. E.g.: `bcim_2016_21_11_2018.gpkg|layername=lim_unidade_federacao_a`  
* *basename*: nome a ser dado à layer no projeto qgis;  
* *providerKey*: "ogr"  
Outros podem ser usados, como -“postgres”, “delimitedtext”, “gpx”, “spatialite”, and “WFS”.  

Exemplo:  

```Python
iface.addVectorLayer("/testes_pyqgis/dados/bcim_2016_21_11_2018.gpkg|layername=lim_unidade_federacao_a", 'lim_unidade_federacao_a', 'ogr' )
```

# 2. Listando camadas de um GEOPACKAGE

FONTE: Adaptado do [STACK EXCHANGE](https://gis.stackexchange.com/questions/307252/qgis-3-python-load-all-layer-from-geopackage)

O formato [GEOPACKAGE](http://www.geopackage.org/) funciona básicamente como um banco de dados que pode ser levado de um lado para o outro, sem precisar instalar um servidor em uma máquina para isso, tendo como base o [SQLITE](https://sqlite.org/index.html).

Uma das formas mais fáceis de acessar um .GPKG é usando a biblioteca [OGR](https://gdal.org/python/osgeo.ogr-module.html):

```python
from osgeo import ogr
```
A biblioteca [OGR](https://gdal.org/python/) da [OSGEO] (https://www.osgeo.org) é utilizada para trabarlhar dados vetoriais.

Em seguida vamos criar uma variável para receber o endereço do .gpkg, isso ajuda por não precisar ficar digitando o caminho o tempo todo... bastando informar a variável que contem activeLayer

:warning: o caminho foi utilizado no LINUX, sendo que no windows e outros poderá ser diferente.

```python
# Caminho completo do arquivo
gpkgPath = "/dados/bcim_2016_21_11_2018.gpkg"
```

Em seguida vamos criar um 'cursor' para conectar no arquivo usando o OGR e conseguir pegar os dados lá dentro

```python
con = ogr.Open(gpkgPath)
```

Para listar os nomes das camadas, basta usar um `for` para mostrar os nomes das camadas.
Nesse momento o metodo  `.GetName()` é o que nos retorna os nomes das camadas:

```python
for i in con:
  print( i.GetName() )
```

Nosso código completo ficaria assim:

```python
from osgeo import ogr

gpkgPath = "/dados/bcim_2016_21_11_2018.gpkg"
con = ogr.Open(gpkgPath)
for i in con:
  print( i.GetName() )
```


## Adicionando as camadas do GPKG ao projeto:

Lembra do iface.addVectorLayer(vectorLayerPath, basename, providerKey )??? visto no modulo anterior? vamos usar ele aqui.


```python
for i in con:
  iface.addVectorLayer(gpkgPath + "|layername="+ i.GetName(), i.GetName(), 'ogr' )
```

Para facilitar ainda mais, podemos transformar os parâmetros informados no addVectorLayer em variáveis e passar apenas as variáves para o metodo, assim o código fica melhor de se ler:

```python
for i in con:
  vectorLayerPath = gpkgPath + "|layername="+ i.GetName()
  basename = i.GetName()
  iface.addVectorLayer(vectorLayerPath, basename, 'ogr' )
```

# 4. Acessando as informações de uma camada.  

Temos duas opções:
  * Atribuir a camada a objeto usando o metodo `QgsProject.instance().mapLayersByName('<nome da camada>')` (veremos mais tarde).
  * Utilizando a camada ativa (camada selecionada e com *underline* no painel camadas) e atribuindo ela a um objeto;  

Vamos pela segunda opção:

* Selecionando camada ativa:  

No Projeto, selecione a camada **lim_unidade_federacao_a**.  
Caso esteja usando um projeto limpo, adicione apenas essa camada ao projeto.  
Ainda usando o `ìface` (me parece que será nosso amigo nessa jornada), vamos usar o *método* `activeLayer()`, atribuindo o resultado a uma objeto. Nessa caso, vmaos chamar de "uf";

```Python
# Atribuindo a camada ativa ao objeto uf
uf = iface.activeLayer()
```

O console não dará nenhum sinal, a não ser que haja algum erro. Uma vez tendo atribuido ao objeto `uf` a camada ativa, podemos seguir brincado...  

**Quantas feições tem essa camda?**:  

```Python
# Contando as feições
uf.featureCount()
```
O console do python apresentará a resposta.  
Perceba que o metodo [`featureCount`](https://qgis.org/pyqgis/master/core/QgsVectorLayer.html?highlight=qgsvectorlayer#qgis.core.QgsVectorLayer.featureCount) pertence ao objeto `qgsVectorLayer`, por isso o usamos sem a necessidade de usar o `iface`.  

**Quais são os campos dessa camada?**:
Para isso, podemos usar o método `fields()`, para poder identificar os campos e iterar sobre eles para, então, usar o método `name()` para termos o nome de cada campo:  

```Python
# Mostrando os campos.
for field in uf.fields():
    print( field.name() )
```  

A gente pode também ter o nome de um campo específico, caso saibamos a ordem no qual mesmo se encontra (lembrando que no python a contagem começa com 0). Então para saber o nome do primeiro campo da camada:  

```Python
# Mostrando o nome do primeiro campo
print(uf.fields()[0].name())
```  

Para abrir a tabela de atributos da camada, teremos que usar o método `showAttributeTable` do `iface`, com o detalhe de incluir o objeto uf no parenteses do método:  

```Python
# Abrindo a tabela de atributos
iface.showAttributeTable(uf)
```  
# 5. Selecionando feições




# 6. Filtrando a camada vetorial.  

Agora que vimos a tabela de atributos, vamos iterar a cada feição existente.  
Vamos usar o método `getFeatures()` do objeto `uf` e iterar sobre ele e
imprimir o campo "nome", afinal, já sabemos que ele existe....

```python
# Exibindo os nomes dentro do campo "nome"
for feature in uf.getFeatures():
  print(feature["nome"])

```  

**Filtrando a camada vetorial por um campo**:  
Sabendo que há uma feição chamada "Rio de Janeiro" no campo "nome" da camada
que estamos trabalhando, podemos filter nossa camada.  
Para isso, vamos usar o método [`setSubsetString()`](https://qgis.org/pyqgis/master/core/QgsVectorLayer.html?highlight=setsubsetstring#qgis.core.QgsVectorLayer.setSubsetString) do objeto `QgsVectorLayer`.  
Nele temos que informar a expressão a ser filtrada
`setSubsetString(self, subset) `:  

```Python
# Filtrando os dados da chamada.
rj = uf.setSubsetString("\"nome\" = 'Rio de Janeiro'")
```

Agora que temos apenas a feição que queremos trabalhar, podemos dar um zoom à ela, usando o método `zoomToActiveLayer()` do `iface`:  

```Python
# Aproximando à camada ativa.
iface.zoomToActiveLayer()
```

Para desfazer o filtro, basta:
```Python
# Limpando a o filtro.
uf.setSubsetString("")
```

# 7. Alterando a simbologia  

Para alterar a cor da feição que estamos trabalhando, vamos usar o comando:  

```python
uf.renderer().symbol().setColor(QColor("black"))
```  
Percebam que o *canvas* precisa ser atualizado. Por isso,
```python
uf.triggerRepaint()
```  

Estamos quase lá. Nos falta atualizar a simbologia na lista de camadas:

```python
iface.layerTreeView().refreshLayerSymbology(uf.id())
```  

# 8. Salvando o projeto;  

Para salvar o projeto que estamos trabalhando, vamos criar uma instancia `[QgsProject](https://qgis.org/pyqgis/master/core/QgsProject.html#qgis.core.QgsProject)` que, por ser uma classe única, basta usar
`instance()` e, então, o método `write()` que deverá informar o caminho da até a pasta a ser salva:  

```python
# Salvando o projeto
QgsProject.instance().write('/projetos/projeto1.qgs')
```
Vejam que após salva-lo e enquanto o mantemos aberto, o nome do projeto pode ser acessado pelo método `fileName()`;  

```python
# Exibindo o nome do arquivo de projeto
print(QgsProject.instance().fileName())
```  

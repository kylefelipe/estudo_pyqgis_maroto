

# Aula 1 - [Quebrando o Gelo](aula_1#quebrando-o-gelo):

  1. [Abrir um gpkg](aula_1#1-abrindo-um-arquivo-geopackage);  
  2. [Listando camadas do gpkg](aula_1/README.md#2-listando-camadas-de-um-geopackage);  
  4. [Acessando informações da camada](aula_1/README.md#4-acessando-as-informações-de-uma-camada);
  5. [Selecionando feições](aula_1/README.md#5-selecionando-feições);
  6. [Filtrando a camada vetorial](aula_1/README.md#6-filtrando-a-camada-vetorial);
  7. [Alterando a simbologia](aula_1/README.md#7-alterando-a-simbologia);
  8. [Salvando o projeto](aula_1/README.md#8-salvando-projeto);

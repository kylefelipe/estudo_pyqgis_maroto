# Estudo PyQgis Maroto

Repositorio criado para iniciar os estudos da API [Python](https:\\www.python.org) do [QGIS](https:\\www.qgis.org)
Iremos usar o QGIS 3 como base, e a notação do Python 3, que é o python padrão do QGIS3

:warning: vamos usar os dados do [banco de dados do IBGE](https://downloads.ibge.gov.br/downloads_geociencias.htm) 1:1.000.000 ().

Que está dentro de :  
    cartas_e_mapas > bcim > versao2016 > geopackage.


Para fazer [download, clique aqui](http://servicodados.ibge.gov.br/Download/Download.ashx?u=geoftp.ibge.gov.br/cartas_e_mapas/bases_cartograficas_continuas/bcim/versao2016/geopackage/bcim_2016_21_11_2018.gpkg), e guarde-o dentro da pasta "/estudo_pyqgis_maroto/dados"  

1. [Primeira aula: "Quebrando o gelo"](estudos/aula_1/README.md#quebrando-o-gelo):  
    1. [Abrir um gpkg](estudos/aula_1#1-abrindo-um-arquivo-geopackage);  
    2. [Listando camadas do gpkg](estudos/aula_1/README.md#2-listando-camadas-de-um-geopackage);  
    4. [Acessando informações da camada](estudos/aula_1/README.md#4-acessando-as-informações-de-uma-camada);
    5. [Selecionando feições](estudos/aula_1/README.md#5-selecionando-feições);
    6. [Filtrando a camada vetorial](estudos/aula_1/README.md#6-filtrando-a-camada-vetorial);
    7. [Alterando a simbologia](estudos/aula_1/README.md#7-alterando-a-simbologia);
    8. [Salvando o projeto](estudos/aula_1/README.md#8-salvando-projeto);
